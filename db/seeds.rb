# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(email: 'admin@admin.com', password: 'admin123', password_confirmation: 'admin123')

names=['Pepe','Carlos','Laura','Juan','Juana','Clara','Andrea','Marcos','David','Maria','Sol']
lastnames=['Perez','Paz','Lopez','Garcia','Pereira','Montenegro','Lescano','Aguilar']

c1 = Course.create(year: 2016)

t11 = Test.create(course: c1, name: 'Primer Parcial', date: Date.new(c1.year, 4, 10), min_grade: 4)
t21 = Test.create(course: c1, name: 'Segundo Parcial', date: Date.new(c1.year, 10, 10), min_grade: 4)
t31 = Test.create(course: c1, name: 'Trabajo Final Integrador', date: Date.new(c1.year, 12, 20), min_grade: 6)

s11 = Student.create(course: c1, first_name: names.sample, last_name: lastnames.sample, dni: '11111111', file_number: '1234/1', email: 'mail@email.com')
s21 = Student.create(course: c1, first_name: names.sample, last_name: lastnames.sample, dni: '22221111', file_number: '1234/2', email: 'mail@email.com')
s31 = Student.create(course: c1, first_name: names.sample, last_name: lastnames.sample, dni: '33331111', file_number: '1234/3', email: 'mail@email.com')
s41 = Student.create(course: c1, first_name: names.sample, last_name: lastnames.sample, dni: '44441111', file_number: '1234/4', email: 'mail@email.com')
s51 = Student.create(course: c1, first_name: names.sample, last_name: lastnames.sample, dni: '55551111', file_number: '1234/5', email: 'mail@email.com')
s61 = Student.create(course: c1, first_name: names.sample, last_name: lastnames.sample, dni: '66661111', file_number: '1234/6', email: 'mail@email.com')


Grade.create(test: t11, student: s11, grade: 7)
Grade.create(test: t11, student: s21, grade: 5)
Grade.create(test: t11, student: s31, grade: 7.5)
Grade.create(test: t11, student: s41, grade: 10)
Grade.create(test: t11, student: s51, grade: 6.5)
Grade.create(test: t11, student: s61, grade: 8)

Grade.create(test: t21, student: s11, grade: 8)
Grade.create(test: t21, student: s21, grade: 5.5)
Grade.create(test: t21, student: s31)
Grade.create(test: t21, student: s41, grade: 2)
Grade.create(test: t21, student: s51, grade: 9)
Grade.create(test: t21, student: s61, grade: 8)

Grade.create(test: t31, student: s11, grade: 9)
Grade.create(test: t31, student: s21, grade: 4)
Grade.create(test: t31, student: s31, grade: 7.5)
Grade.create(test: t31, student: s41, grade: 10)
Grade.create(test: t31, student: s51, grade: 4)
#Grade.create(test: t31, student: s61, grade: 8)


c2 = Course.create(year: 2017)

t12 = Test.create(course: c2, name: 'Primer Parcial', date: Date.new(c2.year, 4, 10), min_grade: 5)
t22 = Test.create(course: c2, name: 'Segundo Parcial', date: Date.new(c2.year, 10, 10), min_grade: 5)


s12 = Student.create(course: c2, first_name: names.sample, last_name: lastnames.sample, dni: '11112222', file_number: '4321/1', email: 'mail@email.com')
s22 = Student.create(course: c2, first_name: names.sample, last_name: lastnames.sample, dni: '22222222', file_number: '4321/2', email: 'mail@email.com')
s32 = Student.create(course: c2, first_name: names.sample, last_name: lastnames.sample, dni: '33332222', file_number: '4321/3', email: 'mail@email.com')
s42 = Student.create(course: c2, first_name: names.sample, last_name: lastnames.sample, dni: '44442222', file_number: '4321/4', email: 'mail@email.com')
s52 = Student.create(course: c2, first_name: names.sample, last_name: lastnames.sample, dni: '55552222', file_number: '4321/5', email: 'mail@email.com')
s62 = Student.create(course: c2, first_name: names.sample, last_name: lastnames.sample, dni: '66662222', file_number: '4321/6', email: 'mail@email.com')
s72 = Student.create(course: c2, first_name: names.sample, last_name: lastnames.sample, dni: '77772222', file_number: '4321/7', email: 'mail@email.com')
s82 = Student.create(course: c2, first_name: names.sample, last_name: lastnames.sample, dni: '88882222', file_number: '4321/8', email: 'mail@email.com')

Grade.create(test: t12, student: s12, grade: 7)
Grade.create(test: t12, student: s22, grade: 5)
Grade.create(test: t12, student: s32, grade: 7.5)
Grade.create(test: t12, student: s42, grade: 10)
Grade.create(test: t12, student: s52, grade: 6.5)
Grade.create(test: t12, student: s62, grade: 8)
Grade.create(test: t12, student: s72, grade: 8)
Grade.create(test: t12, student: s82)

#Grade.create(test: t22, student: s12, grade: 8)
Grade.create(test: t22, student: s22, grade: 5.5)
Grade.create(test: t22, student: s32)
Grade.create(test: t22, student: s42, grade: 2)
Grade.create(test: t22, student: s52, grade: 5)
Grade.create(test: t22, student: s62, grade: 8)
Grade.create(test: t22, student: s72, grade: 9)
Grade.create(test: t22, student: s82, grade: 4)