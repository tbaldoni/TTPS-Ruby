class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :email
      t.string :file_number, limit: 8, null: false
      t.integer :dni, limit: 8, null: false
      t.belongs_to :course, foreign_key: true

      t.timestamps
    end
  end
end
