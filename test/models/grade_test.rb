require 'test_helper'

class GradeTest < ActiveSupport::TestCase
  
  test 'should assert passed test' do
    assert grades(:one).passed?,'the grade passed the test'
  end
  
  test 'should not assert test passed' do
    assert_not grades(:tree).passed?, 'the grade failed the test'
  end

  test 'should not assert absent to test' do
    assert_not grades(:one).absent?, 'the grade was not absent'
  end
  
  
  
end
