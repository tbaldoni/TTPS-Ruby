require 'test_helper'

class CourseTest < ActiveSupport::TestCase

  test 'should save valid course' do
    course = Course.new(year: 2000)
    assert course.save, 'canot save a valid course'
  end

  test 'should not save a course without a year' do
    course = Course.new    
    assert_not course.save, 'saved a course without a year'
  end

  test 'should not save a course with duplicated year' do
    course = Course.new(year: courses(:one).year)
	assert_not course.save, 'saved a course already with duplicate year'
  end

  test 'should not save a course if year is not a number' do
    course = Course.new(year: 'lalal')
    assert_not course.save, 'saved a course with an invalid year'
  end
  
  
end
