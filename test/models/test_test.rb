require 'test_helper'

class TestTest < ActiveSupport::TestCase

setup do
  
  Student.create(first_name:"nuevo",last_name:"nuevo",file_number:"nuevo",dni:11111111,email:"nuevo@nuevo.com",course:courses(:two)) 
  end

  test 'should assert ammount of grades' do
    assert_equal 4, tests(:two).grades.count
  end
  
  test 'should assert ammount of passed' do
    assert_equal 2, tests(:two).ammount_passed
  end
  
  test 'should assert ammount of failed' do
    assert_equal 1, tests(:two).ammount_failed
  end

  test 'should assert ammount of absent' do
    assert_equal 1, tests(:two).ammount_absent
  end
  
   test 'should assert pass_rate' do
    assert_equal 66, tests(:two).pass_rate.to_i
  end
  
=begin
  test 'should concider absent a student added to the course without grade' do
  
  
  puts tests(:two).students.count
  puts courses(:two).students
	s=Student.create(first_name:"nuevo",last_name:"nuevo",file_number:"nuevo",dni:11111111,email:"nuevo@nuevo.com",course:courses(:two)) 
	s.save
	puts tests(:two).students.count
	puts tests(:two).course.students.count
	 puts courses(:two).students
	assert_equal s.course, courses(:two)
	assert_equal tests(:two).course, courses(:two)
	
	assert_equal 1, tests(:two).ammount_absent
	grades=tests(:two).initialize_grades
	puts grades.count
	assert_equal 2, tests(:two).ammount_absent
  end
=end  
end
