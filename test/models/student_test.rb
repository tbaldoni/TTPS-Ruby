require 'test_helper'

class StudentTest < ActiveSupport::TestCase

  test 'should not save empty student' do
    student = Student.new
    assert_not student.save, 'saved an empty student'
  end
  
 
  test 'should not save a duplicated student in the same course' do

	student =Student.new(first_name:"nuevo",last_name:"nuevo",file_number:"nuevo",dni:students(:one).dni,email:"nuevo@nuevo.com",course:courses(:one)) 
	assert_not student.save, 'saved a duplicated student in the same course'
  end

  
  test 'should save a duplicated student in a diferent course' do
    student =Student.new(first_name:"nuevo",last_name:"nuevo",file_number:"nuevo",dni:students(:two).dni,email:"nuevo@nuevo.com",course:courses(:one)) 
	student.valid?
	puts student.errors.full_messages
	assert student.save, 'cannot save a duplicated student in a diferent course'
  end


  test 'should assert passed test' do
    assert students(:one).passed?(tests(:one)),'the student has passed the test'
  end
  
  test 'should not assert test approved' do
    assert_not students(:two).passed?(tests(:tree)), 'the student has failed the test'
  end

  test 'should not assert absent to test' do
    assert_not students(:one).absent_to?(tests(:one)), 'the student was not absent to the test'
  end

  test 'should not assert test approved by a student not in the course of the test' do
	exception = assert_raises(Exception) {
		students(:one).passed?(tests(:tree))
	}
	assert exception.message.include?("no pertenece") , 'the student dont belong to the course of the test'

  end
  
  test 'should not concider absent a student not in the course of the test' do  
	exception = assert_raises(Exception) {
		students(:one).absent_to?(tests(:tree))
	}
	assert exception.message.include?("no pertenece") , 'the student dont belong to the course of the test'
  end

  
  
end
