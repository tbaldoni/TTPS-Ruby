Rails.application.routes.draw do
  devise_for :users
  #resources :grades
  resources :tests do
	member do
      get 'grades'
    end
  end
  resources :students, :except => [:index]
  resources :courses do
	member do
      get 'grades'
    end
  end
  
#	devise_scope :user do
#		root to: "devise/sessions#new"
#	end
  
  root  "courses#index" 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
