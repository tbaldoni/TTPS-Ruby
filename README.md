# TTPS-ruby

## Especificaciones

* Ruby version: 2.4.1
* Rails version: 5.1.4
* Base de datos: PostgreSQL (solo para ambientes productivos)

## Preparacion del ambiente

Una vez clonado el repositorio se debe posicionar sobre el directorio TTPS-ruby y ejecutar:

```bash
$ bin/bundle install
```

Para preparar la base de datos se debe ejecutar:

```bash
$ bin/rails db:create
$ bin/rails db:migrate
```
Si se quieren cargar los datos iniciales a la base se debe ejecutar:

```bash
$ bin/rails db:seed
```

Para levantar el contexto se debe ejecutar:

```bash
$ bin/rails s
```

Si se ejecutaron los seeds se puede acceder al sitio con el usuario:

* Email: `admin@admin.com`
* Contraseña: `admin123`

## Testing

Los test de unidad se pueden ejecutar con el comando:

```bash
$ bin/rails test
```
