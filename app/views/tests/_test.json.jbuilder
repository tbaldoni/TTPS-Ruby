json.extract! test, :id, :name, :date, :min_grade, :course_id, :created_at, :updated_at
json.url test_url(test, format: :json)
