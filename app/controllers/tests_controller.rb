class TestsController < ApplicationController
  before_action :set_test, only: [:show, :edit, :update, :destroy, :grades]

  # GET /tests
  # GET /tests.json
  def index
    @tests = Test.joins(:course).order("courses.year desc, tests.date desc")
  end

  # GET /tests/1
  # GET /tests/1.json
  def show
  end

  # GET /tests/new
  def new
    @test = Test.new
	@test.course =Course.find_by(id: params[:course])
  end

  # GET /tests/1/edit
  def edit
  end

  # POST /tests
  # POST /tests.json
  def create
    @test = Test.new(test_params)

    respond_to do |format|
      if @test.save
		format.html { redirect_to tests_url, notice: 'Test was successfully created.' }
		format.json { head :no_content }
      else
        format.html { render :new }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tests/1
  # PATCH/PUT /tests/1.json
  def update
    respond_to do |format|
      if @test.update(test_params)
		format.html { redirect_to tests_url, notice: 'Test was successfully updated.' }
		format.json { head :no_content }
      else
        format.html { render :edit }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tests/1
  # DELETE /tests/1.json
  def destroy
    @test.destroy
    respond_to do |format|
      format.html { redirect_to tests_url, notice: 'Test was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  # GET /tests/1/grades
  def grades
	@test.initialize_grades
    @course = @test.course
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test
      @test = Test.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def test_params
      params.require(:test).permit(:name, :date, :min_grade, :course_id, 
	  grades_attributes: [:id, :test_id, :student_id, :grade])
    end
end
