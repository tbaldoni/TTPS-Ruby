class Grade < ApplicationRecord
  belongs_to :student
  belongs_to :test
  
  
  validates :student, uniqueness: { scope: :test, message: "El estudiante ya posee una nota para este examen" }  
	
	def passed?
		return false if grade.blank?
		grade>=test.min_grade
	end
	
	def failed?
		return false if grade.blank?
		grade<test.min_grade
	end
	
	def absent?
		grade.blank?
	end
  
  def result
	absent? ? "Ausente" : passed? ? "Aprobado" : "Desaprobado" 
  end
  
end
