class Course < ApplicationRecord
	has_many :students, dependent: :destroy
	has_many :tests, dependent: :destroy
	has_many :grades, through: :tests

	validates :year, uniqueness: {message:"Ya existe un curso para el año ingresado" }, numericality: true 
	
	def to_s
		year.to_s
	end
	
	def empty?
		students.length==0;
	end
	
	def yearsForCombo
		(4.year.ago.year.. 2.year.since.year).reverse_each.map {|v| v }
	end
	
end
