class Student < ApplicationRecord
	belongs_to :course
	has_many :grades, dependent: :destroy

	validates :course, presence: {message: "Debe seleccionar una cursada" }
	validates :first_name, presence: {message: "El nombre es de ingreso obligatorio" }
	validates :last_name, presence: {message: "El apellido es de ingreso obligatorio" }
	validates :dni, presence: {message: "El dni es de ingreso obligatorio" }, numericality: {message: "El dni debe contener solo digitos" }, length: {:is => 8, :wrong_length => "El dni debe tener %{count} caracteres"}, uniqueness: { scope: :course, message: "Ya existe  un estudiante con el dni ingresado" } 
	validates :file_number, presence: {message: "El legajo es de ingreso obligatorio" }, uniqueness: { scope: :course, message: "Ya existe  un estudiante con el lejajo ingresado" } 
	validates :email, format:{ with: /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/ , message: "Formato de mail invalido" }
	
	def to_s
		"#{first_name} #{last_name}"
	end	
	
	def grade_for(test)		
		raise "El estudiante no pertenece al curso del examen" if test.course!=course
		grade=grades.find {|g| g.test == test }
		grade.grade unless grade.nil?
	end

	def absent_to?(test)
		grade_for(test).blank?
	end

	def passed?(test)
		grade_for(test) >= test.min_grade
	end

	def failed?(test)
		grade_for(test) < test.min_grade
	end
	
end
