class Test < ApplicationRecord
  belongs_to :course
  has_many :grades, dependent: :destroy
  accepts_nested_attributes_for :grades
  delegate :students, to: :course
  
  validates :name, presence: {message: "El nombre es de ingreso obligatorio" } 
  validates :date, presence: {message: "La fecha es de ingreso obligatorio" } 
  validates :course, presence: {message: "La cursada es de ingreso obligatorio" } 
  validates :min_grade, presence: {message: "La nota minima es de ingreso obligatorio" } 
  
  scope :ordered, -> {
    joins(:course).order("course.year")
  }
  
	def to_s
		"#{name}-#{course.year}"		
	end
	
	def initialize_grades
		students.each do |s|
			grades.find_or_create_by(test_id: id, student_id: s.id)
		end
	end
	
	def ammount_passed
		grades.select{|g| g.passed?}.count
	end

	def ammount_failed
		grades.select{|g| g.failed?}.count
	end

	def ammount_absent
		grades.select{|g| g.absent?}.count
	end

	def pass_rate
		return 0 if grades.empty?
		rate=(ammount_passed.to_f / (grades.count-ammount_absent).to_f * 100).round(2)
		return 0 if rate.nan?
		rate
	end
end
